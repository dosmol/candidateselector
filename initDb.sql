#Create the roles that user the users
INSERT INTO `candidates_selector`.`role` (`id`, `name`, `description`, `ts`, `tu`) VALUES ('1', 'ADMIN', 'Administrator', '2021-11-30', '2021-11-30');
INSERT INTO `candidates_selector`.`role` (`id`, `name`, `description`, `ts`, `tu`) VALUES ('2', 'USER', 'Limited user', '2021-11-30', '2021-11-30');

#Create the first User wht the default password is 1
INSERT INTO `candidates_selector`.`user` (`id`, `name`, `password`, `ts`, `tu`) VALUES ('1', 'admin', '$2a$12$DYdcHbTTkqzigXAkGTsWBerZpE1S1uNJY/BPhqRgXFiYadGoxU7wa',now(),now());
#Assign the rol to the user.
INSERT INTO `candidates_selector`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '1');


#Create the data for candidates
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('1', '32464065', 'Carrizo', 'Matias', 'Godoy 251', '234234234', '1990-10-22', 'carrizo@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('2', '32464066', 'Pignata', 'Ricardo', 'Gomez 854', '56765765', '1987-12-25', 'Pignata@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('3', '32464067', 'Mellor', 'Lorena', 'Bv. 25 de Mayo 4458', '345345', '1968-9-25', 'Mellor@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('4', '32464068', 'Ramirez', 'Cristian', 'San Martin 200', '78978978', '2003-10-25', 'Ramirez@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('5', '32464069', 'Cabo', 'Sofia', 'Dorrego 15', '34534543', '2001-08-13', 'Cabo@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('6', '32464070', 'Gonzales', 'Alejandra', 'San Luis 85', '867876867867', '1997-05-06', 'Gonzales@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('7', '32464071', 'Rodriguez', 'Beltran', 'Santiago 4587', '111321323', '2005-10-07', 'Rodriguez@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('8', '32464072', 'Combina', 'Nicolas', 'Paraguay 85', '123123312', '1995-06-02', 'Combina@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('9', '32464073', 'Fedele', 'Trsitan', 'Las palmas 412', '43534534', '1996-04-05', 'Fedele@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('10', '32464074', 'Amado', 'Analia', 'San Juan 2', '57567656', '1978-03-08', 'Amado@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('11', '32464075', 'Richieri', 'Pedro', 'Rioja 366', '1234334534', '2004-10-25', 'Richieri@mail.com', now(), now());
INSERT INTO `candidates_selector`.`candidate` (`id`, `dni`, `last_name`, `first_name`, `address`, `phone`, `birthday`, `email`, `ts`, `tu`) VALUES ('12', '32464076', 'Fernandez', 'Lucas', 'Mendoza 1548', '345222344', '1996-10-10', 'Fernandez@mail.com', now(), now());




