package com.challenge.controller;

import com.challenge.Exception.NoCandidateException;
import com.challenge.domain.Candidate;
import com.challenge.dto.*;
import com.challenge.mapper.CandidateMapper;
import com.challenge.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/candidates")
@CrossOrigin
public class CandidateController extends BaseController{

	@Autowired
	private CandidateService candidateService;

	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<CandidatePagedResponse> getCandidates(@RequestParam(defaultValue = "0") int page,
																@RequestParam(defaultValue = "10") int size,
	 															@RequestParam(defaultValue = "") String firstName) {

		//Create a Pageable type object to know the page that want to search, and the size of the block of data
		Pageable paging = PageRequest.of(page, size);
		Page<Candidate> candidates;
		if(firstName.isEmpty()){
			candidates = candidateService.getAllPaged(paging);
		}else{
			candidates = candidateService.getByNamePaged(firstName,paging);
		}


		//Map the Page obtained in the service, to the response desired.
		CandidatePagedResponse candidatePagedResponse = CandidateMapper.toResumeResponse(candidates);

		//Return an OK response, with the mapped data.
		return new ResponseEntity<>(candidatePagedResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<CandidateFullResponse> getCandidate(@PathVariable Long id) {
		//Obtain the data of the candidate with the id.
		Candidate candidate = candidateService.getById(id);

		//If the candidate does not exists, return a not msg.
		if (candidate == null) return ResponseEntity.notFound().build();

		//Map the data to the response, and return an OK response
		CandidateFullResponse candidateResponse = CandidateMapper.toResponse(candidate);
		return new ResponseEntity<>(candidateResponse, HttpStatus.OK);
	}


	//POST METHODS
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@PostMapping
	public ResponseEntity<Object> saveCandidate(@RequestBody CandidateRequest candidateRequest) {

		//Obtain the data of the candidate with the DNI.
		Candidate candidate = this.candidateService.getByDni(candidateRequest.getDni());
		Candidate candidateSaved;
		if(candidate == null){
			//If there is no candidate with the DNI, map the request and save it.
			candidate = CandidateMapper.toDomain(candidateRequest);
			candidateSaved = this.candidateService.save(candidate);
			//Return an OK response, with the saved data.
			return new ResponseEntity<>(CandidateMapper.toResponse(candidateSaved), HttpStatus.OK);
		}else if(candidate.getDeletedDate() != null){
			//If I found a candidate with the DNI, but it's deleted, reactive with new nata and update the candidate data.
			candidate.activate(candidateRequest);
			candidate.setDeletedDate(null);
			candidateSaved = this.candidateService.save(candidate);
			//Return an OK response, with the updated data.
			return new ResponseEntity<>(CandidateMapper.toResponse(candidateSaved), HttpStatus.OK);
		}else{
			//If the candidate exists and it's active, return a error status with a msg.
			return new ResponseEntity<>(new GeneralMsgResponse("Create Candidate", "The candidate you want to add already exists"), HttpStatus.BAD_REQUEST);
		}
	}

	//PUT METHODS
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@PutMapping()
	public ResponseEntity<Object> updateCandidate(@RequestBody CandidateRequest candidateRequest) {
		//Check if the candidateRequest has the id of the candidate to update
		if(candidateRequest.getId() != null){
			//Search the candidate with the ID
			Candidate actualCandidate = this.candidateService.getById(candidateRequest.getId());
			if(actualCandidate!= null){
				//If a candidate is found, update the data, save and return an OK response with the candidate data.
				actualCandidate.updateData(candidateRequest);
				Candidate updatedCandidate = this.candidateService.save(actualCandidate);
				return new ResponseEntity<>(CandidateMapper.toResponse(updatedCandidate), HttpStatus.OK);
			}else{
				//If there is no candidate with the ID, inform it.
				return new ResponseEntity<>(new GeneralMsgResponse("Update Candidate", "The candidate with the ID doesn't exists"), HttpStatus.NOT_FOUND);
			}
		}else{
			//If there is no ID, inform it.
			return new ResponseEntity<>(new GeneralMsgResponse("Update Candidate", "You must to indicate the id of the candidate to update"), HttpStatus.NOT_FOUND);
		}
	}

	//DELETE METHODS
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<GeneralMsgResponse> deleteCandidate(@PathVariable Long id) {
		try{
			//Send the ID to the service, to delete (logically) the candidate. The service can trow a exception if no candidate is found, other way delete it.
			this.candidateService.delete(id);
			return new ResponseEntity<>(new GeneralMsgResponse("Delete Candidate", "The candidate was successfully deleted"), HttpStatus.OK);
		}catch (NoCandidateException nce){
			//If no candidate is found, inform it.
			return new ResponseEntity<>(new GeneralMsgResponse("Delete Candidate", nce.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
