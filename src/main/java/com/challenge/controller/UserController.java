package com.challenge.controller;

import com.challenge.domain.User;
import com.challenge.domain.list.UserList;
import com.challenge.mapper.UserMapper;
import com.challenge.service.UserService;
import com.challenge.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;


@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController extends BaseController{

	@Autowired
	private UserService userService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/getCurrentUser")
	public ResponseEntity<UserResponse> getCurrentUser() {
		//Get the user object that exists in the actual context.
		Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//if there is no user, return a not found msg.
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		//Get the name of the user, to find find the user object.
		org.springframework.security.core.userdetails.User usuario = (org.springframework.security.core.userdetails.User)user;
		String nombre = usuario.getUsername();
		
		//Find the user with that name.
		User usu = this.userService.getUser(nombre);

		//Map the user data.
		UserResponse userResponse = UserMapper.toResponse(usu);
		//Return the mapped data with a OK response.
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<UserResponse> getUser(@PathVariable long id) {
		//Find the user with the specific ID
		final User user = userService.getUser(id);

		//if there is no user, return a not found msg.
		if (user == null) {
			return ResponseEntity.notFound().build();
		}

		//Map the user data.
		UserResponse userResponse = UserMapper.toResponse(user);
		//Return the mapped data with a OK response.
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
	@PreAuthorize("hasRole('ROLE_USER') OR hasRole('ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<ArrayList<UserResponse>> getUsers() {
		//Return a list with the users.
		final UserList users = userService.getUsers();

		//if there is no user, return a not found msg.
		if (users == null) {
			return ResponseEntity.notFound().build();
		}

		//Map the user data.
		ArrayList<UserResponse> userResponse = UserMapper.toResponse(users);
		//Return the mapped data with a OK response.
		return new ResponseEntity<>(userResponse, HttpStatus.OK);
	}
}
