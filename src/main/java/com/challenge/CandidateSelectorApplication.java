package com.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CandidateSelectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidateSelectorApplication.class, args);
	}

}
