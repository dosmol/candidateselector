package com.challenge.repository;

import com.challenge.domain.User;
import com.challenge.domain.list.UserList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

@Component
public interface UserRepository extends JpaRepository<User, Long> {
	public UserList findAll();

	@Query("SELECT u FROM User u WHERE u.name = :name AND u.fechaEliminacion = NULL")
	public User findByName(@Param("name") String  name);

	public User findById(long id);
}
