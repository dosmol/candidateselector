package com.challenge.repository;

import com.challenge.domain.Candidate;
import com.challenge.domain.list.CandidateList;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.data.domain.Pageable;

@Component
public interface CandidateRepository extends JpaRepository<Candidate, Long>, CrudRepository<Candidate,Long> {
	public CandidateList findAll();
	public Page<Candidate> findByDeletedDateIsNull(Pageable pageable);
	public Page<Candidate> findByFirstNameContainingAndDeletedDateIsNull(String firstName,Pageable pageable);
	public Candidate findByIdAndDeletedDateIsNull(long id);
	public Candidate findByDni(String dni);
}
