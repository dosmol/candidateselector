package com.challenge.repository;

import com.challenge.domain.Role;
import com.challenge.domain.list.RoleList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	public RoleList findAll();

	Role getRoleById(long id);

	Role findByName(String name);
}
