package com.challenge.domain;

import com.challenge.dto.CandidateRequest;
import com.challenge.utils.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@AllArgsConstructor
@Data
public class Candidate implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidate_generator")
	@SequenceGenerator(name = "candidate_generator", initialValue = 100)
	private long id;
	@Column
	private String dni;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private Date birthday;
	@Column
	private String address;
	@Column
	private String phone;
	@Column
	private String email;
	@Column
	private LocalDateTime deletedDate;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;

	public Candidate() {
	}

	public Candidate(String dni, String firstName, String lastName, Date birthday, String address, String phone, String email, LocalDateTime ts, LocalDateTime tu) {
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}
	public Candidate(CandidateRequest candidateRequest){
		this.dni = candidateRequest.getDni();
		this.firstName = candidateRequest.getFirstName();
		this.lastName = candidateRequest.getLastName();
		this.address = candidateRequest.getAddress();
		this.phone = candidateRequest.getPhone();
		this.email = candidateRequest.getEmail();
		this.birthday = DateUtil.stringToDate(candidateRequest.getBirthday());

	}

	public void updateData(CandidateRequest candidateRequest)  {
		this.id = candidateRequest.getId();
		this.dni = candidateRequest.getDni();
		this.firstName = candidateRequest.getFirstName();
		this.lastName = candidateRequest.getLastName();
		this.address = candidateRequest.getAddress();
		this.phone = candidateRequest.getPhone();
		this.email = candidateRequest.getEmail();
		this.birthday = DateUtil.stringToDate(candidateRequest.getBirthday());
	}
	public void activate(CandidateRequest candidateRequest)  {
		this.dni = candidateRequest.getDni();
		this.firstName = candidateRequest.getFirstName();
		this.lastName = candidateRequest.getLastName();
		this.address = candidateRequest.getAddress();
		this.phone = candidateRequest.getPhone();
		this.email = candidateRequest.getEmail();
		this.birthday = DateUtil.stringToDate(candidateRequest.getBirthday());
		this.setDeletedDate(null);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

	public LocalDateTime getTs() {
		return ts;
	}

	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}

	public LocalDateTime getTu() {
		return tu;
	}

	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}

	private static final long serialVersionUID = 1L;

	
}
