package com.challenge.domain;

import com.challenge.dto.AuthorizationRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@Data
public class User implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name = "user_generator", initialValue = 10)
	private long id;
	@Column
	private String name;
	@Column
	private String email;
	@Column
	private String password;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "USER_ROLES", joinColumns = {
			@JoinColumn(name = "USER_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ROLE_ID") })
	private Set<Role> roles;
	@Column
	private LocalDateTime fechaEliminacion;
	@Column
	@CreationTimestamp
	private LocalDateTime ts;
	@Column
	@UpdateTimestamp
	private LocalDateTime tu;

	public User(String name, String email) {
		super();
		this.name = name;
		this.email = email;
		this.roles = new HashSet<Role>();
	}

	public User(String name, String password, Set<Role> roles) {
		super();
		this.name = name;
		this.password = password;
		this.roles = roles;
	}

	public User() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}

	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	public LocalDateTime getTs() {
		return ts;
	}

	public void setTs(LocalDateTime ts) {
		this.ts = ts;
	}

	public LocalDateTime getTu() {
		return tu;
	}

	public void setTu(LocalDateTime tu) {
		this.tu = tu;
	}
	
	public Boolean isAdmin() {
		for (Role rol : this.roles) {
			if(rol.getName().equals("ADMIN")) {
				return true;
			}
		}
		return false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private static final long serialVersionUID = 1L;

	
}
