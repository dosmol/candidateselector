package com.challenge.mapper;

import com.challenge.domain.Candidate;
import com.challenge.domain.User;
import com.challenge.domain.list.UserList;
import com.challenge.dto.*;
import com.challenge.dto.list.CandidateFullResponseList;
import com.challenge.dto.list.CandidateResumeResponseList;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class CandidateMapper {

	private CandidateMapper() {
	}

	public static CandidateFullResponse toResponse(Candidate candidate) {
		return new CandidateFullResponse(candidate);
	}
	public static Candidate toDomain(CandidateRequest candidateRequest){
		return new Candidate(candidateRequest);
	}
	public static CandidatePagedResponse toResponse(Page<Candidate> pages){
		return new CandidatePagedResponse(pages);
	}
	public static CandidateFullResponseList toResponse(List<Candidate> candidates){
		CandidateFullResponseList responseList = new CandidateFullResponseList();
		for (Candidate candidate:candidates) {
			responseList.add(CandidateMapper.toResponse(candidate));
		}
		return responseList;
	}

	public static CandidatePagedResponse toResumeResponse(Page<Candidate> pages){
		return new CandidatePagedResponse(pages);
	}
	public static CandidateResumeResponse toResumeResponse(Candidate candidate){
		return new CandidateResumeResponse(candidate);
	}
	public static CandidateResumeResponseList toResumeResponse(List<Candidate> candidates){
		CandidateResumeResponseList responseList = new CandidateResumeResponseList();
		for (Candidate candidate:candidates) {
			responseList.add(CandidateMapper.toResumeResponse(candidate));
		}
		return responseList;
	}
}
