package com.challenge.mapper;

import com.challenge.domain.User;
import com.challenge.domain.list.UserList;
import com.challenge.dto.AuthorizationRequest;
import com.challenge.dto.UserResponse;

import java.util.ArrayList;

public class UserMapper {

	private UserMapper() {
	}

	public static UserResponse toResponse(User user) {
		return new UserResponse(user);	
	}
	public static ArrayList<UserResponse> toResponse(UserList users) {
		ArrayList<UserResponse> finalUsers=new ArrayList<>();
		for (User user : users) {
			finalUsers.add(new UserResponse(user));
		}
		return finalUsers;
	}

	public static User toDomain(AuthorizationRequest authorizationRequest) {
		return new User(authorizationRequest.getUser(),authorizationRequest.getEmail());
	}
}
