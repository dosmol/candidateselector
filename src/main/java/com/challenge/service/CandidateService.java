package com.challenge.service;

import com.challenge.Exception.NoCandidateException;
import com.challenge.domain.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CandidateService{
	public Candidate getById(Long id);
	public Candidate getByDni(String dni);
	public Page<Candidate> getAllPaged(Pageable pageable);
	public Page<Candidate> getByNamePaged(String firstName, Pageable pageable);
	public Candidate save(Candidate candidate);
	public Boolean delete(Long id) throws NoCandidateException;
}
