package com.challenge.service.impl;

import com.challenge.Exception.NoCandidateException;
import com.challenge.domain.Candidate;
import com.challenge.repository.CandidateRepository;
import com.challenge.service.CandidateService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Optional;

@Service("candidateService")
public class CandidateServiceImpl implements CandidateService {

	private CandidateRepository candidateRepository;

	public CandidateServiceImpl(CandidateRepository candidateRepository) {
		this.candidateRepository = candidateRepository;
	}

	@Override
	public Page<Candidate> getAllPaged(Pageable pageable){
		return this.candidateRepository.findByDeletedDateIsNull(pageable);
	}
	@Override
	public Page<Candidate> getByNamePaged(String firstName, Pageable pageable){
		return this.candidateRepository.findByFirstNameContainingAndDeletedDateIsNull(firstName,pageable);
	}

	@Override
	public Candidate getById(Long id){
		return this.candidateRepository.findById(id).get();
	}

	@Override
	public Candidate getByDni(String dni){return this.candidateRepository.findByDni(dni);}

	@Override
	public Candidate save(Candidate candidate){
		return this.candidateRepository.save(candidate);
	}

	@Override
	public Boolean delete(Long id) throws NoCandidateException {
		//Find the candidate which is not deleted.
		Candidate candidate = this.candidateRepository.findByIdAndDeletedDateIsNull(id);
		if(candidate != null){
			//If the candidate is found, set the deleteDate and save it.
			candidate.setDeletedDate(LocalDateTime.now());
			this.candidateRepository.save(candidate);
		}else{
			//If the candidate is not found, a custom exception is throw.
			throw new NoCandidateException("The candidate you want to delete doesn't exist");
		}
		return true;
	}
}
