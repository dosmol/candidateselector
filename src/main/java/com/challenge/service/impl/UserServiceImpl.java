package com.challenge.service.impl;

import com.challenge.domain.User;
import com.challenge.domain.list.UserList;
import com.challenge.mapper.UserDetailsMapper;
import com.challenge.repository.UserRepository;
import com.challenge.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("userDetailsService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		final User retrievedUser = userRepository.findByName(userName);
		if (retrievedUser == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}

		return UserDetailsMapper.build(retrievedUser);
	}

	@Override
	public User getUser(long id) {
		return userRepository.findById(id);
	}
	
	@Override
	public User getUser(String userName) {
		return userRepository.findByName(userName)
;	}
	
	@Override
	public UserList getUsers() {
		return userRepository.findAll();
	}
}
