package com.challenge.service;

import com.challenge.domain.User;
import com.challenge.domain.Role;
import com.challenge.domain.list.UserList;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {

	User getUser(long id);
	User getUser(String userName);
	UserList getUsers();
}
