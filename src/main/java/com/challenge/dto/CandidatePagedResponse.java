package com.challenge.dto;

import com.challenge.domain.Candidate;
import com.challenge.dto.list.CandidateFullResponseList;
import com.challenge.dto.list.CandidateResumeResponseList;
import com.challenge.mapper.CandidateMapper;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;

@Data
public class CandidatePagedResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private CandidateResumeResponseList candidates;
	private Integer currentPage;
	private Long totalItems;
	private Integer totalPages;

	public CandidatePagedResponse(Page<Candidate> pages) {
		this.candidates = CandidateMapper.toResumeResponse(pages.getContent());
		this.totalPages = pages.getTotalPages();
		this.totalItems = pages.getTotalElements();
		this.currentPage = pages.getNumber();
	}
}
