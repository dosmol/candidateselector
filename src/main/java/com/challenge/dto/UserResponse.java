package com.challenge.dto;

import com.challenge.domain.Role;
import com.challenge.domain.User;
import com.challenge.dto.list.RolResponseList;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class UserResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;
	private RolResponseList roles;
	private LocalDateTime fechaEliminacion;
	private String rolesString="";
	private String email;

	
	public UserResponse(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public UserResponse(Long id, String name, LocalDateTime fechaEliminacion) {
		super();
		this.id = id;
		this.name = name;
		this.fechaEliminacion = fechaEliminacion;
	}
	public UserResponse(User user) {
		super();
		this.id = user.getId();
		this.name = user.getName();
		this.email=user.getEmail();
		this.roles = new RolResponseList();
		if(user.getRoles()!=null) {
			for (Role rol : user.getRoles()) {
				this.roles.add(new RolResponse(rol));
				this.rolesString += this.rolesString.isEmpty()?rol.getDescription():", "+rol.getDescription();
			}
		}
		this.fechaEliminacion = user.getFechaEliminacion();
	}

	public UserResponse() {
		super();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDateTime getFechaEliminacion() {
		return fechaEliminacion;
	}
	public void setFechaEliminacion(LocalDateTime fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	public RolResponseList getRoles() {
		return roles;
	}

	public void setRoles(RolResponseList roles) {
		this.roles = roles;
	}

	public String getRolesString() {
		return rolesString;
	}

	public void setRolesString(String rolesString) {
		this.rolesString = rolesString;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
