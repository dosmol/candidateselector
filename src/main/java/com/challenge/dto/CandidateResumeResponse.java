package com.challenge.dto;

import com.challenge.domain.Candidate;
import com.challenge.utils.DateUtil;
import lombok.Data;

import java.io.Serializable;

@Data
public class CandidateResumeResponse implements Serializable {
	private static final long serialVersionUID = 1L;


	private String dni;
	private String firstName;
	private String lastName;


	public CandidateResumeResponse() {
	}
	public CandidateResumeResponse(Candidate candidate){
		this.dni = candidate.getDni();
		this.firstName = candidate.getFirstName();
		this.lastName = candidate.getLastName();
	}
	public CandidateResumeResponse(String dni, String firstName, String lastName) {
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
