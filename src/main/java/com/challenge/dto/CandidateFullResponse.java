package com.challenge.dto;

import com.challenge.domain.Candidate;
import com.challenge.domain.Role;
import com.challenge.domain.User;
import com.challenge.dto.list.RolResponseList;
import com.challenge.utils.DateUtil;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class CandidateFullResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String dni;
	private String firstName;
	private String lastName;
	private String birthday;
	private String address;
	private String phone;
	private String email;

	public CandidateFullResponse() {
	}
	public CandidateFullResponse(Candidate candidate){
		this.id = candidate.getId();
		this.dni = candidate.getDni();
		this.firstName = candidate.getFirstName();
		this.lastName = candidate.getLastName();
		this.birthday = DateUtil.dateToString(candidate.getBirthday());
		this.address = candidate.getAddress();
		this.phone = candidate.getPhone();
		this.email = candidate.getEmail();
	}
	public CandidateFullResponse(long id, String dni, String firstName, String lastName, String birthday, String address, String phone, String email) {
		this.id = id;
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
