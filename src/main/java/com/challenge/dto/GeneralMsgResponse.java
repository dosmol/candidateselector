package com.challenge.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class GeneralMsgResponse {
	private String titulo;
	private ArrayList<String> mensajes;

	public GeneralMsgResponse() {
		super();
	}
	public GeneralMsgResponse(String titulo, ArrayList<String> mensajes) {
		super();
		this.titulo = titulo;
		this.mensajes = mensajes;
	}
	public GeneralMsgResponse(String titulo, String mensajes) {
		super();
		this.titulo = titulo;
		this.mensajes = new ArrayList<>();
		this.mensajes.add(mensajes);
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public ArrayList<String> getMensajes() {
		return mensajes;
	}
	public void setMensajes(ArrayList<String> mensajes) {
		this.mensajes = mensajes;
	}
	
	

}
