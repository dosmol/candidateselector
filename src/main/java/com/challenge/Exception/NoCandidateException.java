package com.challenge.Exception;

public class NoCandidateException extends Exception {
    public NoCandidateException(String errorMessage) {
        super(errorMessage);
    }
}
