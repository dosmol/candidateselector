package com.challenge.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtil {

	public static Date stringToDate(String date) {
		//Return the LocalDateTime from the string given
		try{
			Date date1= new SimpleDateFormat("dd/MM/yyyy").parse(date);
			return date1;
		}catch (ParseException e){
			return null;
		}
	};

	public static String dateToString(Date date) {
		//Return the LocalDateTime from the string given
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(date);
	};

}
